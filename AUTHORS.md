# Authors

Documentation authors and translators for the SATIE project includes (alphabetical order of last name):

* Nicolas Bouillot
* Emmanuel Durand
* Marc Lavallée
* Thomas Piquet
* Michał Seta
* Edith Viau
